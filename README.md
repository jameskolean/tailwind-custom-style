# Tailwind custom style example

## Create project 

```shell
npm create astro@latest tailwind-custom-style
cd tailwind-custom-style
npx astro add tailwind
```

## Run project

```shell
npm install
npm run dev
open http://localhost:3000
```
